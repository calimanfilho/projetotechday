#!/bin/bash

# echo "Criando os Secrets..."

# kubectl apply -f ./secrets.yml

echo "Criando o Persistent Volume (PV)..."

kubectl apply -f ./pv.yml

echo "Criando o Persistent Volume Claim (PVC)..."

kubectl apply -f ./pvc.yml

echo "Criando os serviços..."

kubectl apply -f ./services.yml

echo "Criando os deployments..."

kubectl apply -f ./deployments.yml