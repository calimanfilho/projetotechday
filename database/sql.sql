USE meubanco;

CREATE TABLE mensagens (
    id          INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome        VARCHAR(50),
    email       VARCHAR(50),
    comentario  VARCHAR(100)
);

SET character_set_client = utf8;
SET character_set_connection = utf8;
SET character_set_results = utf8;
SET collation_connection = utf8_general_ci;