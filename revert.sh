#!/bin/bash

echo "Deletando os deployments..."

kubectl delete deployment mysql
kubectl delete deployment php

echo "Deletando os pods..."

kubectl delete --all pods --namespace=default

# echo "Deletando os serviços..."

# kubectl delete service php
# kubectl delete service mysql-connection

echo "Deletando o Persistent Volume (PV)..."

kubectl delete pv nfs-server

echo "Deletando o Persistent Volume Claim (PVC)..."

kubectl delete pvc mysql-data