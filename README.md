# Projeto Final do Cloud DevOps Experience Banco Carrefour
Esse projeto foi desenvolvido conforme as etapas informadas no [material de apoio](http://bit.ly/3tGI95H) da apresentação [Tech Day: Dicas para o Sucessso do seu Projeto](https://youtu.be/JwVavHXRwj0).

## Contextualizando o Projeto
A aplicação [banco-carrefour-techday-cloud-devops](https://github.com/digitalinnovationone/banco-carrefour-techday-cloud-devops) foi disponibilizada para ser usada no projeto, essa aplicação utiliza no _frontend_ as linguagens HTML, CSS e JavaScript, e no _backend_ PHP e MySQL.

Com a aplicação em mãos, foi criado um _pipeline_ de _deploy_ da aplicação em forma de _containers_ em um _cluster_ Kubernetes, o cluster Kubernetes foi feito em nuvem (Google Cloud Platform — GCP). O _pipeline_ de CI/CD foi feito no Gitlab.

Ao fim, foi feito um [vídeo](https://youtu.be/pn3U6bvRpQM) com o pit da solução.

A estrutura básica do projeto seguiu as especificações da imagem abaixo:

![Estrutura Básica do Projeto](images/basic-structure.png)

## Alterando o _Backend_
Para a aplicação funcionar corretamente foi necessário alterar o código do [index.php](backend/indexphp):

```
header("Access-Control-Allow-Headers: *");
```

Para

```
header("Access-Control-Allow-Headers: Content-Type");
```

Pois o `Access-Control-Allow-Headers` não recebe `*` como valor.

Outra modificação feita, foi na `query` de inserção dos dados ao banco de dados, o campo `ID` agora está sendo incrementado automáticamente, e não há mais necessidade de receber um número aleatório. Também foi corrigida a indentação dos códigos.

> ℹ️ **_INFO_**
> Uma futura melhoria, seria não divulgar os dados de acesso ao banco de dados no arquivo [conexao.php](backend/conexao.php).

## Alterando o _Database_
Para evitar que o _pipeline_ de CI/CD mostre qual a senha e o nome do banco de dados, o `build` da imagem do _database_ não será automatizado, mas os arquivos que seriam utilizados para o `build`, continuarão disponíveis no repositório para aprendizado.

Essa etapa foi feita manualmente, acessando o `pod` pelo `Bastion Host` e executando as _querys_ disponívels no arquivo [sql.sql](database/sql.sql). Foram realizadas algumas alterações nessas _querys_, como a transformação do ID em chave primária, com um valor que não seja nulo e que seja incrementado automaticamente. E a inclusão de comandos `SET`, esses comandos são responsáveis por habilitar os caracteres `utf8` no MySQL, por padrão essas variáveis da imagem são definidas como `latin1`, isso faz com que o banco não reconheça caracteres `unicode`.

> ℹ️ **_INFO_**
> Uma futura melhoria, seria não divulgar os dados de acesso ao banco de dados no arquivo [dockerfile](database/dockerfile).

## Alterando o _Frontend_
No _frontend_ deve ser alterado o URL de acesso a aplicação para o IP do `Load Balancer`, e o URL dos ícones de nome, e-mail e comentário para o caminho das imagens no próprio repositório, pois não estavam mais disponíveis nos sites de origens. Também foi corrigida a indentação dos códigos.

## Criando o Repositório Remoto
O GitLab foi escolhido para a criação do _pipeline_ de CI/CD. O primeiro passo foi criar o repositório remoto no [GitLab](https://gitlab.com/) e colocar o projeto [banco-carrefour-techday-cloud-devops](https://github.com/digitalinnovationone/banco-carrefour-techday-cloud-devops) nele.

## Criando o Cluster
Para esse projeto foi criado o cluster pelo console do GCP, alterando o **Nome** do cluster para `meu-cluster` e em `default-pool` > `nós` foi alterado o **Tipo de disco de inicialização** para `Disco permanente padrão` devido ao problema de `insufficient regional quota`, as outras configurações foram as padrões do Kubernetes Engine, como, a **Zona** em `us-central-c`, **Número de nós** em `3` e o **Tipo da máquina** `e2-medium`.

<details>
  <summary>Informações do Cluster</summary>

Item | Descrição
-----|----------
Nome | meu-cluster	
Tipo de Local | Por Zona	
Zona do Plano de Controle | us-central1-c	
Zonas de Nós Padrão | us-central1-c
Canal de Lançamento | Canal Regular	
Versão | 1.24.8-gke.2000	
Tamanho Total | 3	
Endpoint Externo | 34.69.149.237	
Endpoint Interno | 10.128.0.3

</details>

## Criando as Chaves Pública e Privada
As chaves pública e privadas foram utilizadas para acessar as máquinas virtuais no GCP, esse procedimento no Windows deve ser feito com o programa PuTTYgen, após aberto, deve-se clicar em `Generate` e mover o mouse no espaço branco para gerar as chaves, após a chave pública ser gerada, é necessário alterar o texto `rsa-key-AAAAMMDD` encontrado ao final da chave para `gcp`, posteriormente essa chave foi utilizado para acesso ao Bastion Host. Ainda no PuTTYgen, deve ser salvo a chave privada, clicando em `Save private key`.

## Criando o Bastion Host
O Bastion Host é uma máquina virtual com acesso a rede onde está o cluster Kubernetes, foi utilizada na etapa de CI/CD para realizar o gerenciamento do cluster. Para a criação do Bastion Host foi utilizado o código abaixo:

```bash
gcloud compute instances create bastion-host \
    --zone=us-central1-c \
    --machine-type=e2-micro \
    --network-interface=network-tier=PREMIUM,subnet=default
```

Foi necessário acessar as configurações da instância pelo console, ir em `EDITAR`, em `Segurança e acesso` na `Chaves SSH`, clicar em `ADICIONAR ITEM` e inserir a chave SSH pública feita na seção anterior. O próximo passo foi abrir o PuTTY (programa para acesso SSH do Windows), inserir o IP público do Bastion Host em `Host Name` e adicionar a chave privada em `Connection` > `SSH` > `Auth` > `Credentials` > em `Private key file for authentication`. Feito isso o acesso remoto ao Bastion Host poderá ser realizado, usando o usuário `gcp`.

Para que o Bastion Host funcione corretamente foi necessário realizar a seguinte configuração:

```bash
gcloud config set account calimanfilho.01@gmail.com
gcloud auth login
gcloud components install gke-gcloud-auth-plugin
gcloud container clusters get-credentials meu-cluster --zone us-central1-c --project potent-result-374121
sudo apt-get install kubectl
```

> ℹ️ **_INFO_**
> Os passos acima são necessários para que não ocorra a mensagem `The connection to the server localhost:8080 was refused - did you specify the right host or port?`.

## Criando o Servidor NFS
Foi utilizado o serviço _Filestore_ do Google para criar o servidor NFS, em **ID da instância** foi informado `nfs-server`, **Região** como `us-central1` e **Zona** como `us-central1-c`, a **Rede VPC** como `default`, **Nome do compartilhamento de arquivos** como `vol1`, as outras configurações foram as padrões do _Filestore_, como, **Tipo de instância** como `Básico`, **Tipo de armazenamento** como `HDD`, **Alocar capacidade** como `1 TB`, **Intervalo de IP alocado** como `Usar um intervalo de IP alocado automaticamente` e **Controles de acesso** como `Permitir acesso a todos os clientes na rede VPC`.

Após a criação foi obtido o IP público do servidor NFS (`10.107.33.194`), para ser colocado no arquivo [pv.yml](pv.yml).

<details>
  <summary>Informação do Servidor NFS</summary>

ID da Instância | Nome do Compartilhamento de Arquivos | Data/Hora de Criação | Nível de Serviço | Local | Endereço IP | Capacidade 
----------------|--------------------------------------|----------------------|------------------|-------|-------------|------------
nfs-server | vol1 | 24 de jan. de 2023 21:22:55 | BASIC_HDD | us-central1-c | 10.107.33.194 | 1 TiB

</details>

## Criando o Secrets
O arquivo [secrets.yml](secrets.yml) foi enviado para esse repositório, apenas para que seja visto pela avaliação, em um ambiente de produção o arquivo [secrets.yml](secrets.yml) seria criado, executado e excluído do Bastion Host, para que os dados sensíveis não fosse visto por pessoas não autorizadas, assim, somente a variável informada no arquivo seria utilizada pela equipe de desenvolvimento. Caso não fosse um problema deixar esses dados do banco de dados expostos, o [dockerfile](database/dockerfile) do _database_ poderia ter as variáveis declaradas, atualmente as variáveis estão comentadas para servir de exemplo, mas como o repositório é público elas não deveriam estar expostas.

## Criando os Arquivos de Configuração
Todos os arquivos de configuração forem feitos a partir de exemplos, e alterados para que atenda aos requisitos do projeto. Foi criado um arquivo para cada tipo de _kind_, [deployments.yml](deployments.yml), [pv.yml](pv.yml), [pvc.yml](pvc.yml) e [services.yml](services.yml).

## Criando o Arquivo de CI/CD
Ao ser executado o _pipeline_ de CI/CD realiza a construção e o _push_ da imagem do contêiner, para isso, foi criado dentro da pasta backend, o [dockerfile](backend/dockerfile), após a criação, para o correto funcionamento do arquivo de CI, [.gitlab-ci.yml](.gitlab-ci.yml), foi criado no repositório remoto as variáveis `REGISTRY_USER`, `REGISTRY_PASS`, `SSH_KEY` e `SSH_SERVER`, acessando `Settings` > `CI/CD` > `Variables (Expand)` > `Add variable`. Após a imagem do contêiner estar disponível no Docker Hub, é feito o  _deploy_ da aplicação no GCP, executando todos os arquivos de configuração no Cluster, através do [script.sh](script.sh).

## Estrutura Final do Projeto
```
.
├── backend
|   ├── conexao.php
|   ├── dockerfile
|   └── index.php
├── database
|   ├── dockerfile
|   └── sql.sql
├── frontend
|   ├── images
|   |   ├── comment.svg
|   |   ├── email.svg
|   |   └── name.svg
|   ├── css.css
|   ├── index.html
|   └── js.js
├── images
|   └── basic-structure.png
├── .gitlab-ci.yml
├── deployment.yml
├── pv.yml
├── pvc.yml
├── README.md
├── revert.sh
├── script.sh
├── secrets.yml
└── services.yml
```

## Resolução de Problemas (Dicas)
Para agilizar os testes das alterações nos códigos, é ideal configurar o cluster na máquina local, para isso, foi utilizado o procedimento abaixo:

> ℹ️ **_INFO_**
> Caso esteja sendo utilizado WSL, a descompactação ficará extremamente lenta se estiver sendo feita em um diretório no Windows.

```bash
curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-414.0.0-linux-x86_64.tar.gz
tar -xf google-cloud-cli-414.0.0-linux-x86_64.tar.gz
./google-cloud-sdk/install.sh
./google-cloud-sdk/bin/gcloud init
```

Após o procedimento acima, foi feito a autenticação por e-mail e selecionado o projeto. E por fim, foi feita a instalação de mais algumas ferramentas e a conexão ao cluster.

```bash
sudo apt-get install google-cloud-sdk-gke-gcloud-auth-plugin
gcloud container clusters get-credentials meu-cluster --zone us-central1-c --project potent-result-374121
sudo apt-get install kubectl
```

Caso seja necessário excluir algum recursos que tenha entrado em loop (status `Terminating` indefinidamente), é necessário utilizar o código:

```bash
kubectl patch <resource> <resource_name> -p '{"metadata":{"finalizers":null}}'
kubectl delete <resource> <resource_name> --grace-period=0 --force
```

Para deletar todos os `pods` do _namespace_ `default`, pode ser utilizado:

```bash
kubectl delete --all pods --namespace=default
```

Para a realização de _troubleshooting_ de algum `pod`, pode ser utilizado o comando:

```bash
kubectl logs <pod-name>
```

Por fim, foi criado o script [revert.sh](revert.sh) para deletar somente os recursos que poderiam estar conflitando.